FROM ubuntu:bionic

RUN apt update \
  && apt install -y openjdk-11-jdk maven nodejs npm \
  && npm install -g newman \
  && npm install -g newman-reporter-html \
  && ln -s /usr/lib/jvm/java-11-openjdk-amd64 /usr/lib/jvm/default-jdk

RUN apt install -y curl \
  && curl -O http://ftp.tc.edu.tw/pub/Apache/tomcat/tomcat-9/v9.0.20/bin/apache-tomcat-9.0.20.tar.gz \
  && tar -zxvf apache-tomcat-9.0.20.tar.gz -C /opt \
  && ln -s /opt/apache-tomcat-9.0.20 /opt/tomcat

ENV CATALINA_HOME=/opt/tomcat \
    JAVA_HOME=/usr/lib/jvm/default-jdk \
    PATH=/opt/tomcat/bin:/usr/lib/jvm/default-jdk/bin:$PATH


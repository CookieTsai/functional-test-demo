# TWJUG Functional Testing Demo

This is a simple java project for TWJUG meeting. Using apache maven build it you can get a war file.

## Project be created by Maven

**Create Command**

```bash
$ mvn archetype:generate \
-DarchetypeGroupId=org.apache.maven.archetypes \
-DarchetypeArtifactId=maven-archetype-webapp \
-DarchetypeVersion=1.4 \
-DgroupId=tw.cookie.twjug \
-DartifactId=demo \
-Dversion=1.0
```

**Build Command**

```bash
$ mvn clean package
```

## Dockerfile

```dockerfile
FROM ubuntu:bionic

RUN apt update \
  && apt install -y openjdk-11-jdk maven nodejs npm \
  && npm install -g newman \
  && npm install -g newman-reporter-html \
  && ln -s /usr/lib/jvm/java-11-openjdk-amd64 /usr/lib/jvm/default-jdk

RUN apt install -y curl \
  && curl -O http://ftp.tc.edu.tw/pub/Apache/tomcat/tomcat-9/v9.0.20/bin/apache-tomcat-9.0.20.tar.gz \
  && tar -zxvf apache-tomcat-9.0.20.tar.gz -C /opt \
  && ln -s /opt/apache-tomcat-9.0.20 /opt/tomcat

ENV CATALINA_HOME=/opt/tomcat \
    JAVA_HOME=/usr/lib/jvm/default-jdk \
    PATH=/opt/tomcat/bin:/usr/lib/jvm/default-jdk/bin:$PATH
```

* CookieTsai 的手記: 淺談Dockerfile. [Link](https://tsai-cookie.blogspot.com/2019/01/dockerfile.html)

## Build Docker Image

```bash
$ docker build -t cookietsai/twjug-demo-tomcat .
```

* You can find this image from docker hub. [Link](https://cloud.docker.com/repository/docker/cookietsai/twjug-demo-tomcat)

## GitLab CI/CD with .gitlab-ci.yml

```yaml
image: cookietsai/twjug-demo-tomcat:latest

stages:
  - build
  - test

variables:
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

cache:
  key: "$CI_BUILD_NAME-$CI_JOB_NAME-cache"
  paths:
    - .m2/repository/

build:twjug-demo:
  stage: build
  script:
    - mvn clean package
  artifacts:
    name: "artifacts-$CI_BUILD_NAME-$CI_JOB_NAME"
    paths:
      - target/demo.war
    expire_in: 30 mins

test:newman:
  stage: test
  before_script:
    - mv target/demo.war /opt/tomcat/webapps/
    - startup.sh
    - sleep 1s
  script:
    - newman run newman/demo.postman_collection.json -e newman/Testing.postman_environment.json -r cli,html --reporter-html-template newman/htmlreqres.hbs --reporter-html-export out/htmlResults.html -n 3 || echo 'finished'
  dependencies:
    - build:twjug-demo
  artifacts:
    name: "artifacts-$CI_BUILD_NAME-$CI_JOB_NAME"
    paths:
      - out/
    expire_in: 30 mins
```

* GitLab CI/CD Quick Start. [Link](https://docs.gitlab.com/ee/ci/quick_start/)
* GitLab Configuration's README. [Link](https://docs.gitlab.com/ee/ci/yaml/README.html)
* GitLab CI job permission. [Link](https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html)

## Make Your Executor

**Install GitLab Runner Command**

```bash
$ docker run -d --name my-gitlab-runner --restart always \
   -v ~/Shared/gitlab-runner/config:/etc/gitlab-runner \
   -v /var/run/docker.sock:/var/run/docker.sock \
   gitlab/gitlab-runner:latest
```

**Register Runner Command**

```bash
$ gitlab-runner register
```

<pre style="background-color: #EFE;">
Runtime platform                                    arch=amd64 os=linux pid=57 revision=f100a208 version=11.6.0
Running in system-mode.

Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
<font color="#D66">https://gitlab.com/</font>

Please enter the gitlab-ci token for this runner
<font color="#D66">xxx</font>

Please enter the gitlab-ci description for this runner
[hostname] <font color="#D66">my-runner</font>

Please enter the gitlab-ci tags for this runner (comma separated):
<font color="#D66">tag1,tag2</font>

Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
<font color="#D66">docker</font>

Please enter the Docker image (eg. ruby:2.1):
<font color="#D66">cookietsai/twjug-demo-tomcat:latest</font>

</pre>
